import { AxiosBuilder } from "./axios.builder";

const Instances = {
  public: AxiosBuilder.create()
    .withBaseUrl("/something")
    .withHeaders({
      "Content-Type": "application/json",
    })
    .toDomain()
    .initInstance(),
  private: AxiosBuilder.create()
    .withBaseUrl("/something")
    .withHeaders({
      "Content-Type": "application/json",
    })
    .haveCredentials()
    .toDomain()
    .initInstance(),
};

export { Instances };
