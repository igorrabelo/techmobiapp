import { createDrawerNavigator } from "@react-navigation/drawer";

export type RootStackParamList = {
  Welcome: undefined;
  Login: undefined;
  Home: undefined;
  User: undefined;
  Notification: undefined;
  Search: undefined;
  Tickets: undefined;
};

export const Drawer = createDrawerNavigator<RootStackParamList>();
