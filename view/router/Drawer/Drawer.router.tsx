import { Drawer } from "./Drawer";
import { RouterProperties as ROUTES } from "../Router.properties";
import React from "react";
import CustomDrawer from "../../components/CustomDrawer/CustomDrawer";
import Home from "../../pages/Home/Home";

const DrawerRouter = () => {

  return (
    <Drawer.Navigator
      initialRouteName="Welcome"
      screenOptions={{ drawerPosition: "right", drawerStyle: { width: 250 } }}
      backBehavior="history"
      drawerContent={(props) => <CustomDrawer {...props} />}
    >
      <Drawer.Screen
        name="Login"
        component={ROUTES.Login}
        options={{ headerShown: false }}
      />
      <Drawer.Screen
        name="Welcome"
        component={ROUTES.Welcome}
        options={{ headerShown: false }}
      />
      <Drawer.Screen
        name="Home"
        component={Home}
        options={{ headerShown: false }}
      />
      <Drawer.Screen
        name="Tickets"
        component={ROUTES.Tickets}
        options={{ headerShown: false }}
      />
      <Drawer.Screen
        name="Search"
        component={ROUTES.Search}
        options={{ headerShown: false }}
      />
      <Drawer.Screen
        name="Notification"
        component={ROUTES.Notification}
        options={{ headerShown: false }}
      />
      <Drawer.Screen
        name="User"
        component={ROUTES.User}
        options={{ headerShown: false }}
      />
    </Drawer.Navigator>
  );
};

export default DrawerRouter;
