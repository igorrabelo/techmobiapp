import Home from "../pages/Home/Home";
import Login from "../pages/Login/Login";
import Notification from "../pages/Notification/Notification";
import Search from "../pages/Search/Search";
import Tickets from "../pages/Tickets/Tickets";
import User from "../pages/User/User";
import Welcome from "../pages/Welcome/Welcome";

export const RouterProperties = {
  Login,
  Welcome,
  Home,
  Tickets,
  Search,
  Notification,
  User,
};

export type RouterPropertyNames = keyof typeof RouterProperties;
