import { StyleSheet } from "react-native";
import { deviceHeight, deviceWidth } from "../../static/Dimensions";
import { Colors } from "../../static/Colors";
import { StaticFonts } from "../../static/Fonts";

export const styles = StyleSheet.create({
  container: {
    width: deviceWidth,
    height: deviceHeight,
    backgroundColor: Colors.main,
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "flex-end",
    position: "relative",
  },
  content: {
    width: deviceWidth,
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignContent: "center",
    gap: 15,
  },
  welcomeText: {
    width: deviceWidth,
    fontSize: deviceWidth / 15,
    color: Colors.mainContrast,
    textAlign: "center",
    fontFamily: StaticFonts[500],
  },
  presentationText: {
    width: deviceWidth,
    fontSize: 14,
    fontFamily: StaticFonts[400],
    lineHeight: deviceHeight / 40,
    paddingHorizontal: 30,
    color: Colors.mainContrast,
    textAlign: "center",
  },
  buttonContainer: {
    width: "100%",
    marginTop: 10,
    paddingHorizontal: 70,
    display: "flex",
    justifyContent: "center",
    alignContent: "center",
  },
  logo: {
    width: deviceWidth / 2,
    height: deviceHeight / 7,
    position: "relative",
    left: "40%",
    transform: [{ translateX: -50 }],
  },
  human: {
    width: deviceWidth,
    height: deviceHeight / 2,
    position: "relative",
    objectFit: "contain",
  },
});
