import { useCallback } from "react";
import { NavigationProps } from "../../extensions/Navigation.type";
import { useNavigation } from "@react-navigation/native";

const UseWelcomeController = () => {
  const navigation = useNavigation<NavigationProps>();

  const navigateToLogin = useCallback(() => {
    navigation.navigate("Login");
  }, []);

  return {
    Actions: {
      onNavigate: navigateToLogin,
    },
  };
};

export default UseWelcomeController;
