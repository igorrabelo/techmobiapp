import { StyleSheet, Text, View } from "react-native";
import React from "react";
import BaseNavigationView from "../../components/BaseNavigationView/BaseNavigationView";

const Search = () => {
  return (
    <BaseNavigationView routerName="Pesquisa">
      <Text>Search</Text>
    </BaseNavigationView>
  );
};

export default Search;

const styles = StyleSheet.create({});
