import { Text, TouchableOpacity, View, Image } from "react-native";
import React from "react";
import BaseNavigationView from "../../components/BaseNavigationView/BaseNavigationView";
import { styles } from "./User.styles";
import UseUserController from "./User.controller";
import {
  EarPhonesDefaultIcon,
  InfoDefaultIcon,
  LockDefaultIcon,
  LogoutDefaultIcon,
} from "../../components/Icons/Icons";
import { Colors } from "../../static/Colors";
import ViewCard from "../../components/VIewCard/ViewCard";

const User = () => {
  const { Actions } = UseUserController();
  return (
    <BaseNavigationView routerName="Profile">
      <ViewCard>
        <View style={styles.profileComponent}>
          <View style={styles.personalInformationTextsComponent}>
            <Text style={styles.emailText}>Sobansaeed.870@gmail.com</Text>
            <Text style={styles.nameText}>Soban Saeed</Text>
            <Text style={styles.statusText}>Driver</Text>
          </View>
          <View style={styles.settingsContainer}>
            <Text style={styles.settingsTitle}>Settings</Text>
            <TouchableOpacity style={styles.touchableSettings}>
              <LockDefaultIcon color={Colors.info} size={18} />
              <Text style={styles.settingsText}>Privacy & Security</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableSettings}>
              <EarPhonesDefaultIcon color={Colors.info} size={18} />
              <Text style={styles.settingsText}>Help and support</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableSettings}>
              <InfoDefaultIcon color={Colors.info} size={18} />
              <Text style={styles.settingsText}>About</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableSettings}>
              <LogoutDefaultIcon color={Colors.info} size={18} />
              <Text style={styles.settingsText}>Logout</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ViewCard>
    </BaseNavigationView>
  );
};

export default User;
