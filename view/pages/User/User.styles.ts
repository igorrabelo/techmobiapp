import { StyleSheet } from "react-native";
import { deviceHeight, deviceWidth } from "../../static/Dimensions";
import { StaticFonts } from "../../static/Fonts";
import { Colors } from "../../static/Colors";

export const styles = StyleSheet.create({
  profileComponent: {
    width: deviceWidth,
    flex:1,
    backgroundColor: Colors.mainContrast,
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    gap: 20,
  },
  personalInformationTextsComponent: {
    maxWidth: "90%",
    width: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    gap: 10,
  },
  emailText: {
    fontSize: 14,
    color: Colors.info,
    fontFamily: StaticFonts[400],
  },
  nameText: {
    fontSize: 25,
    color: "black",
    fontFamily: StaticFonts[700],
  },
  statusText: {
    fontSize: 16,
    color: Colors.main,
    fontFamily: StaticFonts[400],
  },
  personalInformationProfilePickComponent: {
    width: "30%",
    display: "flex",
    position: "relative",
  },
  imageContainer: {
    width: "100%",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderColor: Colors.main,
    borderWidth: 2,
    borderRadius: 25,
  },
  image: {
    width: "90%",
    height: "90%",
    borderRadius: 25,
    objectFit: "contain",
  },
  iconOpacity: {
    position: "absolute",
    width: deviceWidth / 9,
    height: deviceWidth / 9,
    backgroundColor: Colors.main,
    bottom: -5,
    left: -5,
    zIndex: 1,
    borderRadius: 12,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  aboutMeContainer: {
    maxWidth: "90%",
    width: "100%",
    height: "20%",
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    gap: 10,
  },
  aboutMeTitle: {
    fontSize: 16,
    fontFamily: StaticFonts[700],
  },
  aboutMe: {
    fontSize: 14,
    fontFamily: StaticFonts[400],
    color: Colors.info,
  },
  settingsContainer: {
    maxWidth: "90%",
    flex: 1,
    width: "100%",
    height: "50%",
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    gap: 15,
  },
  settingsTitle: {
    fontSize: 16,
    fontFamily: StaticFonts[700],
  },
  touchableSettings: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    gap: 15,
  },
  settingsText: {
    fontSize: 15,
    fontFamily: StaticFonts[500],
    color: Colors.info,
  },
});
