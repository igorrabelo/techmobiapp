import { useCallback } from "react";
import { NavigationProps } from "../../extensions/Navigation.type";
import { useNavigation } from "@react-navigation/native";

const UseNotificationController = () => {
  const navigation = useNavigation<NavigationProps>();

  const navigateForLastRoute = useCallback(() => {
    if (navigation.canGoBack()) {
      navigation.goBack();
    }
  }, []);

  return {
    Actions: {
      onGoBack: navigateForLastRoute,
    },
    States: {},
  };
};

export default UseNotificationController;
