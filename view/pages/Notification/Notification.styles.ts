import { StyleSheet } from "react-native";
import { deviceHeight, deviceWidth } from "../../static/Dimensions";
import { StaticFonts } from "../../static/Fonts";

export const styles = StyleSheet.create({
  ticketDayAmount: {
    maxWidth: "90%",
    width: "100%",
    fontSize: 20,
    fontFamily: StaticFonts[700],
    textAlign: "justify",
    marginVertical: 20,
  },
  ticketCardContainer: {
    width: "100%",
    height: deviceHeight,
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    gap: 10,
    marginBottom: deviceHeight / 7.5,
  },
});
