import { Text, View } from "react-native";
import React from "react";
import BaseNavigationView from "../../components/BaseNavigationView/BaseNavigationView";
import UseNotificationController from "./Notification.controller";
import { TicketCard } from "../../components/TicketsCard/TicketCards";
import { EVehicleType } from "../../../business/domain/enums/EVehicleType";
import TruckImage from "../../../assets/truck.jpg";
import { styles } from "./Notification.styles";
import { ScrollView } from "react-native-gesture-handler";
import ViewCard from "../../components/VIewCard/ViewCard";

const Notification = () => {
  const { Actions, States } = UseNotificationController();
  return (
    <BaseNavigationView routerName="Notificações">
      <ViewCard>
        {[1, 2, 3, 4, 5].length >= 5 ? (
          <ScrollView>
            <View style={styles.ticketCardContainer}>
              <Text style={styles.ticketDayAmount}>Today</Text>
              <TicketCard.Container onPress={() => {}} haveBorder>
                <TicketCard.TicketImage source={TruckImage} />
                <TicketCard.Content
                  ticketNumber={"Ticket#2348"}
                  ticketStatus="Overweight Detected for the ticket"
                  createdAt={new Date()}
                  vehicle={{
                    type: EVehicleType.Truck,
                    vehiclePlate: "IJN 8947",
                  }}
                />
              </TicketCard.Container>
              <TicketCard.Container onPress={() => {}} haveBorder>
                <TicketCard.TicketImage source={TruckImage} />
                <TicketCard.Content
                  ticketNumber={"Ticket#2348"}
                  ticketStatus="Overweight Detected for the ticket"
                  createdAt={new Date()}
                  vehicle={{
                    type: EVehicleType.Truck,
                    vehiclePlate: "IJN 8947",
                  }}
                />
              </TicketCard.Container>
              <TicketCard.Container onPress={() => {}} haveBorder>
                <TicketCard.TicketImage source={TruckImage} />
                <TicketCard.Content
                  ticketNumber={"Ticket#2348"}
                  ticketStatus="Overweight Detected for the ticket"
                  createdAt={new Date()}
                  vehicle={{
                    type: EVehicleType.Truck,
                    vehiclePlate: "IJN 8947",
                  }}
                />
              </TicketCard.Container>
              <Text style={styles.ticketDayAmount}>This Month</Text>
              <TicketCard.Container onPress={() => {}} haveBorder>
                <TicketCard.TicketImage source={TruckImage} />
                <TicketCard.Content
                  ticketNumber={"Ticket#2348"}
                  ticketStatus="Overweight Detected for the ticket"
                  createdAt={new Date()}
                  vehicle={{
                    type: EVehicleType.Truck,
                    vehiclePlate: "IJN 8947",
                  }}
                />
              </TicketCard.Container>
              <TicketCard.Container onPress={() => {}} haveBorder>
                <TicketCard.TicketImage source={TruckImage} />
                <TicketCard.Content
                  ticketNumber={"Ticket#2348"}
                  ticketStatus="Overweight Detected for the ticket"
                  createdAt={new Date()}
                  vehicle={{
                    type: EVehicleType.Truck,
                    vehiclePlate: "IJN 8947",
                  }}
                />
              </TicketCard.Container>

              <TicketCard.Container onPress={() => {}} haveBorder>
                <TicketCard.TicketImage source={TruckImage} />
                <TicketCard.Content
                  ticketNumber={"Ticket#2348"}
                  ticketStatus="Overweight Detected for the ticket"
                  createdAt={new Date()}
                  vehicle={{
                    type: EVehicleType.Truck,
                    vehiclePlate: "IJN 8947",
                  }}
                />
              </TicketCard.Container>
              <TicketCard.Container onPress={() => {}} haveBorder>
                <TicketCard.TicketImage source={TruckImage} />
                <TicketCard.Content
                  ticketNumber={"Ticket#2348"}
                  ticketStatus="Overweight Detected for the ticket"
                  createdAt={new Date()}
                  vehicle={{
                    type: EVehicleType.Truck,
                    vehiclePlate: "IJN 8947",
                  }}
                />
              </TicketCard.Container>
              <TicketCard.Container onPress={() => {}} haveBorder>
                <TicketCard.TicketImage source={TruckImage} />
                <TicketCard.Content
                  ticketNumber={"Ticket#2348"}
                  ticketStatus="Overweight Detected for the ticket"
                  createdAt={new Date()}
                  vehicle={{
                    type: EVehicleType.Truck,
                    vehiclePlate: "IJN 8947",
                  }}
                />
              </TicketCard.Container>
            </View>
          </ScrollView>
        ) : (
          <View style={styles.ticketCardContainer}>
            <TicketCard.Container onPress={() => {}} haveBorder>
              <TicketCard.TicketImage source={TruckImage} />
              <TicketCard.Content
                ticketNumber={"Ticket#2348"}
                ticketStatus="Overweight Detected for the ticket"
                createdAt={new Date()}
                vehicle={{
                  type: EVehicleType.Truck,
                  vehiclePlate: "IJN 8947",
                }}
              />
            </TicketCard.Container>
          </View>
        )}
      </ViewCard>
    </BaseNavigationView>
  );
};

export default Notification;
