import { StyleSheet } from "react-native";
import { deviceHeight, deviceWidth } from "../../static/Dimensions";

export const styles = StyleSheet.create({
  ticketCardContainer: {
    width: "100%",
    height: deviceHeight,
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    gap: 10,
    marginBottom: deviceHeight / 9
  },
});
