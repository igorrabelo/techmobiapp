import { View } from "react-native";
import { styles } from "./Home.styles";
import { ScrollView } from "react-native-gesture-handler";
import { TicketCard } from "../../components/TicketsCard/TicketCards";
import { EVehicleType } from "../../../business/domain/enums/EVehicleType";
import React from "react";
import ViewCard from "../../components/VIewCard/ViewCard";
import TruckImage from "../../../assets/truck.jpg";
import UseHomeController from "./Home.controller";
import BaseNavigationView from "../../components/BaseNavigationView/BaseNavigationView";

const Home = () => {
  const { Actions, States } = UseHomeController();
  return (
    <BaseNavigationView routerName="Eventos">
      <ViewCard>
        {[1, 2, 3, 4, 5].length >= 5 ? (
          <ScrollView>
            <View style={styles.ticketCardContainer}>
              <TicketCard.Container onPress={() => {}} haveBorder>
                <TicketCard.TicketImage source={TruckImage} />
                <TicketCard.Content
                  ticketNumber={"Id#2348"}
                  ticketStatus="Overweight Detected for the ticket"
                  createdAt={new Date()}
                  vehicle={{
                    type: EVehicleType.Truck,
                    vehiclePlate: "IJN 8947",
                  }}
                />
              </TicketCard.Container>
              <TicketCard.Container onPress={() => {}} haveBorder>
                <TicketCard.TicketImage source={TruckImage} />
                <TicketCard.Content
                  ticketNumber={"Id#2348"}
                  ticketStatus="Overweight Detected for the ticket"
                  createdAt={new Date()}
                  vehicle={{
                    type: EVehicleType.Truck,
                    vehiclePlate: "IJN 8947",
                  }}
                />
              </TicketCard.Container>
              <TicketCard.Container onPress={() => {}} haveBorder>
                <TicketCard.TicketImage source={TruckImage} />
                <TicketCard.Content
                  ticketNumber={"Id#2348"}
                  ticketStatus="Overweight Detected for the ticket"
                  createdAt={new Date()}
                  vehicle={{
                    type: EVehicleType.Truck,
                    vehiclePlate: "IJN 8947",
                  }}
                />
              </TicketCard.Container>
              <TicketCard.Container onPress={() => {}} haveBorder>
                <TicketCard.TicketImage source={TruckImage} />
                <TicketCard.Content
                  ticketNumber={"Id#2348"}
                  ticketStatus="Overweight Detected for the ticket"
                  createdAt={new Date()}
                  vehicle={{
                    type: EVehicleType.Truck,
                    vehiclePlate: "IJN 8947",
                  }}
                />
              </TicketCard.Container>
              <TicketCard.Container onPress={() => {}} haveBorder>
                <TicketCard.TicketImage source={TruckImage} />
                <TicketCard.Content
                  ticketNumber={"Id#2348"}
                  ticketStatus="Overweight Detected for the ticket"
                  createdAt={new Date()}
                  vehicle={{
                    type: EVehicleType.Truck,
                    vehiclePlate: "IJN 8947",
                  }}
                />
              </TicketCard.Container>
              <TicketCard.Container onPress={() => {}} haveBorder>
                <TicketCard.TicketImage source={TruckImage} />
                <TicketCard.Content
                  ticketNumber={"Id#2348"}
                  ticketStatus="Overweight Detected for the ticket"
                  createdAt={new Date()}
                  vehicle={{
                    type: EVehicleType.Truck,
                    vehiclePlate: "IJN 8947",
                  }}
                />
              </TicketCard.Container>
              <TicketCard.Container onPress={() => {}} haveBorder>
                <TicketCard.TicketImage source={TruckImage} />
                <TicketCard.Content
                  ticketNumber={"Id#2348"}
                  ticketStatus="Overweight Detected for the ticket"
                  createdAt={new Date()}
                  vehicle={{
                    type: EVehicleType.Truck,
                    vehiclePlate: "IJN 8947",
                  }}
                />
              </TicketCard.Container>
              <TicketCard.Container onPress={() => {}} haveBorder>
                <TicketCard.TicketImage source={TruckImage} />
                <TicketCard.Content
                  ticketNumber={"Id#2348"}
                  ticketStatus="Overweight Detected for the ticket"
                  createdAt={new Date()}
                  vehicle={{
                    type: EVehicleType.Truck,
                    vehiclePlate: "IJN 8947",
                  }}
                />
              </TicketCard.Container>
            </View>
          </ScrollView>
        ) : (
          <View style={styles.ticketCardContainer}>
            <TicketCard.Container onPress={() => {}} haveBorder>
              <TicketCard.TicketImage source={TruckImage} />
              <TicketCard.Content
                ticketNumber={"Id#2348"}
                ticketStatus="Overweight Detected for the ticket"
                createdAt={new Date()}
                vehicle={{
                  type: EVehicleType.Truck,
                  vehiclePlate: "IJN 8947",
                }}
              />
            </TicketCard.Container>
          </View>
        )}
      </ViewCard>
    </BaseNavigationView>
  );
};

export default Home;
