import { useMemo } from "react";
import UseFormTouch from "../../extensions/UseFormTouch";
import { useForm, SubmitHandler } from "react-hook-form";
import { NavigationProps } from "../../extensions/Navigation.type";
import { useNavigation } from "@react-navigation/native";

type HomeFormType = {
  location: string;
  search: string;
};

const UseHomeController = () => {
  const { touch, touched } = UseFormTouch();
  const { handleSubmit, control } = useForm<HomeFormType>();

  const navigation = useNavigation<NavigationProps>();

  const locationItems = useMemo((): Array<{
    label: string;
    value: unknown;
  }> => {
    return [
      {
        label: "Halal Lab office",
        value: 1,
      },
      {
        label: "Halal Lab work",
        value: 2,
      },
      {
        label: "Halal Lab",
        value: 3,
      },
      {
        label: "home",
        value: 4,
      },
    ];
  }, []);

  return {
    Actions: {},
    States: {
      navigation,
      locationItems,
    },
    Helpers: {
      control,
    },
  };
};

export default UseHomeController;
