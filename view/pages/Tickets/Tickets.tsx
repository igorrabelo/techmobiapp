import { StyleSheet, Text, View } from "react-native";
import React from "react";
import BaseNavigationView from "../../components/BaseNavigationView/BaseNavigationView";
import UseTicketsController from "./Tickets.controller";

const Tickets = () => {
  const { Actions, States } = UseTicketsController();
  return (
    <BaseNavigationView routerName="Tickets">
      <Text>Tickets</Text>
    </BaseNavigationView>
  );
};

export default Tickets;
