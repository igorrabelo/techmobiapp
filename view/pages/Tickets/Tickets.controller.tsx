import { NavigationProps } from "../../extensions/Navigation.type";
import { useNavigation } from "@react-navigation/native";

const UseTicketsController = () => {
  const navigation = useNavigation<NavigationProps>();

  return {
    Actions: {},
    States: {},
  };
};

export default UseTicketsController;
