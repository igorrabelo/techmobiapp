import { View, Text, Image } from "react-native";
import WhiteLogo from "../../../assets/techmobi_white_logo.png";
import React from "react";
import UseLoginController from "./Login.controller";
import { styles } from "./Login.styles";
import CustomText from "../../components/Input/CustomTextInput/CustomText";
import { LockIcon, UserIcon } from "../../components/Icons/Icons";
import CustomButton from "../../components/CustomButton/CustomButton";

const Login = () => {
  const { Actions, Helpers } = UseLoginController();
  return (
    <View style={styles.component}>
      <View style={styles.waveView}>
        <Image source={WhiteLogo} style={styles.logo} />
      </View>
      <View style={styles.textView}>
        <View style={styles.form}>
          <Text style={styles.formTitle}>Sign in</Text>
          <Text style={styles.formExplanationText}>
            Loren ipsum dolor ist amet, consectetur adipiscing elit, sed do
            eisumd
          </Text>
          <CustomText
            name="email"
            control={Helpers.control}
            errors={Helpers.errors}
            Icon={<UserIcon />}
            placeholder="Email"
          />
          <CustomText
            name="password"
            textContentType="password"
            secureTextEntry
            control={Helpers.control}
            errors={Helpers.errors}
            Icon={<LockIcon />}
            placeholder="Password"
          />
          <CustomButton title="SIGN IN" onPress={Actions.onSubmit} />
        </View>
      </View>
    </View>
  );
};

export default Login;
