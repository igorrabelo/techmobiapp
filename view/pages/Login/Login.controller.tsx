import { useCallback } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { LoginFormSchema } from "./Login.validation";
import { useNavigation } from "@react-navigation/native";
import { NavigationProps } from "../../extensions/Navigation.type";
import { LoginService } from "../../../business/service/login.service";
type LoginFormType = {
  email: string;
  password: string;
};

const UseLoginController = () => {
  const loginService = new LoginService();
  const navigation = useNavigation<NavigationProps>();
  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<LoginFormType>({
    resolver: yupResolver(LoginFormSchema),
  });

  const navigateHome = useCallback(() => {
    return navigation.navigate("Home");
  }, []);

  const onSubmit: SubmitHandler<LoginFormType> = useCallback(async (data) => {
    await loginService.login(data.email, data.password);
  }, []);

  return {
    Actions: {
      onSubmit: handleSubmit(onSubmit),
      onNavigateHome: navigateHome,
    },
    Helpers: {
      errors,
      control,
    },
  };
};

export default UseLoginController;
