import { Colors } from "../../static/Colors";
import { deviceHeight, deviceWidth } from "../../static/Dimensions";
import { StyleSheet } from "react-native";
import { StaticFonts } from "../../static/Fonts";

export const styles = StyleSheet.create({
  component: {
    width: deviceWidth,
    height: deviceHeight,
    flex: 1,
    backgroundColor: "white",
  },
  logo: {
    width: deviceWidth / 2,
    height: deviceHeight / 7,
    zIndex: 2,
    position: "absolute",
    left: "37%",
    top: "50%",
    transform: [{ translateX: -50 }, { translateY: -50 }],
  },
  waveView: {
    backgroundColor: Colors.main,
    width: deviceWidth,
    height: deviceHeight / 3.6,
    position: "relative",
  },
  textView: {
    marginTop: 30,
    width: deviceWidth,
    height: deviceHeight,
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  form: {
    width: "80%",
    margin: "auto",
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    gap: 20,
  },
  formTitle: {
    fontSize: 32,
    fontFamily: StaticFonts[700],
  },
  formExplanationText: {
    textAlign: "center",
    lineHeight: 22,
    fontSize: 15,
    fontFamily: StaticFonts[400],
    color: Colors.info,
  },
  newAccountSectionComponent: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    gap: 5,
  },
  newAccountExplanationText: {
    fontSize: 15,
    fontFamily: StaticFonts[400],
    color: Colors.info,
  },
  newAccountNavigatorText: {
    fontSize: 15,
    fontFamily: StaticFonts[400],
    color: Colors.main,
    textDecorationLine: "underline",
  },
});
