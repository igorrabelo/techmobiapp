import { useCallback, useState } from "react";

const UseFormTouch = () => {
  const [touched, setTouched] = useState<boolean>(false);

  const touch = useCallback(() => {
    if (!touched) {
      setTouched(true);
    }
  }, []);

  return {
    touched,
    touch,
  };
};

export default UseFormTouch;
