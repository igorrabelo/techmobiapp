export const StaticFonts = {
  400: "DMSans_18pt-Black",
  500: "DMSans_24pt-Black",
  700: "DMSans_36pt-Black",
};
