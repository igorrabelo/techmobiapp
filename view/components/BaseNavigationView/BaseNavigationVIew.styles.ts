import { StyleSheet } from "react-native";
import { deviceHeight, deviceWidth } from "../../static/Dimensions";
import { Colors } from "../../static/Colors";
import { StaticFonts } from "../../static/Fonts";

export const styles = StyleSheet.create({
  container: {
    width: deviceWidth,
    height: deviceHeight,
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Colors.main,
  },
  goBackNavigationContainer: {
    flex: 1,
  },
  navigationContainer: {
    maxWidth: "90%",
    width: "100%",
    height: deviceHeight / 9,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end",
    gap: deviceWidth / 14,
    paddingBottom: 10,
  },
  routeText: {
    flex: 1,
    textAlign: "center",
    fontSize: 16,
    color: Colors.mainContrast,
    fontFamily: StaticFonts[500],
  },
  rightIcons: {
    flex: 1,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    gap: 10,
  },
});
