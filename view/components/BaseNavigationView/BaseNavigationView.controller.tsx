import { useCallback, useMemo } from "react";
import { NavigationProps } from "../../extensions/Navigation.type";
import { useRoute, useNavigation } from "@react-navigation/native";
import { Colors } from "../../static/Colors";
import { deviceWidth } from "../../static/Dimensions";
import { CommonActions } from "@react-navigation/native";

const UseBaseNavigationViewController = () => {
  const { name } = useRoute();
  const navigation = useNavigation<NavigationProps>();

  const defaultIconSize = useMemo(() => {
    return deviceWidth / 16;
  }, []);

  const currentRouteIsNotHome = useMemo(() => {
    return name !== "Home";
  }, [name]);

  const mainIconColor = useMemo(() => {
    return Colors.mainContrast;
  }, []);

  const openDrawer = useCallback(() => {
    navigation.openDrawer();
  }, []);
  const closeDrawer = useCallback(() => {
    navigation.closeDrawer();
  }, []);

  const navigateSearch = useCallback(() => {
    closeDrawer();
    navigation.navigate("Search");
  }, []);

  const navigateBack = useCallback(() => {
    if (navigation.canGoBack()) {
      closeDrawer();
      navigation.dispatch(CommonActions.goBack());
    }
  }, []);

  return {
    Actions: {
      openDrawer,
      onNavigateSearch: navigateSearch,
      onNavigateBack: navigateBack,
      onCloseDrawer: closeDrawer,
    },
    States: {
      iconSize: defaultIconSize,
      mainIconColor,
      currentRouteIsNotHome,
    },
  };
};

export default UseBaseNavigationViewController;
