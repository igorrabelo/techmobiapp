import { View, Text } from "react-native";
import React from "react";
import UseBaseNavigationViewController from "./BaseNavigationView.controller";
import { styles } from "./BaseNavigationVIew.styles";
import {
  EllipsisDefaultIcon,
  FilterDefaultIcon,
  SearchIcon,
  ArrowDefaultIcon,
} from "../Icons/Icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import { DrawerContentComponentProps } from "@react-navigation/drawer";

interface BaseNavigationViewProps {
  routerName: string;
  children: React.ReactNode;
}

const BaseNavigationView = ({
  children,
  routerName,
}: BaseNavigationViewProps) => {
  const { Actions, States } = UseBaseNavigationViewController();
  return (
    <View style={styles.container}>
      <View style={styles.navigationContainer}>
        <View style={styles.goBackNavigationContainer}>
          {States.currentRouteIsNotHome && (
            <TouchableOpacity onPress={Actions.onNavigateBack}>
              <ArrowDefaultIcon
                size={States.iconSize}
                color={States.mainIconColor}
              />
            </TouchableOpacity>
          )}
        </View>
        <Text style={styles.routeText}>{routerName}</Text>
        <View style={styles.rightIcons}>
          <TouchableOpacity>
            <FilterDefaultIcon
              size={States.iconSize}
              color={States.mainIconColor}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={Actions.onNavigateSearch}>
            <SearchIcon size={States.iconSize} color={States.mainIconColor} />
          </TouchableOpacity>
          <TouchableOpacity onPress={Actions.openDrawer}>
            <EllipsisDefaultIcon
              size={States.iconSize}
              color={States.mainIconColor}
            />
          </TouchableOpacity>
        </View>
      </View>
      {children}
    </View>
  );
};

export default BaseNavigationView;
