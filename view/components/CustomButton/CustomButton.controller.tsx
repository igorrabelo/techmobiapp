import { useCallback } from "react";
import { styles } from "./CustomButton.styles";

const UseCustomButtonController = ({
  variant,
}: {
  variant?: "default" | "transparent";
}) => {
  const manageStyle = useCallback(() => {
    if (!variant || variant === "default") {
      return styles.buttonDefault;
    }
    return styles.buttonWhite;
  }, []);

  return {
    Actions: {
      manageStyle,
    },
  };
};

export default UseCustomButtonController;
