import { ButtonProps } from "react-native";
import React from "react";
import { TouchableOpacity, Text } from "react-native";
import { styles } from "./CustomButton.styles";
import UseCustomButtonController from "./CustomButton.controller";

interface Props extends ButtonProps {
  title: string;
  onPress: () => void;
  variant?: "default" | "transparent";
}

const CustomButton = ({ title, onPress, variant, ...otherProps }: Props) => {
  const { Actions } = UseCustomButtonController({ variant });

  return (
    <TouchableOpacity
      style={Actions.manageStyle()}
      onPress={onPress}
      {...otherProps}
    >
      <Text style={styles.text}>{title}</Text>
    </TouchableOpacity>
  );
};

export default CustomButton;
