import { View } from "react-native";
import React from "react";
import { styles } from "./ViewCard.styles";

type ViewCardProps = {
  children: React.ReactNode | React.ReactNode[];
};

const ViewCard = ({ children }: ViewCardProps) => {
  return <View style={styles.component}>{children}</View>;
};

export default ViewCard;
