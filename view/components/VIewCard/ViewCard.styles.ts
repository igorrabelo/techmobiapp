import { StyleSheet } from "react-native";
import { deviceHeight, deviceWidth } from "../../static/Dimensions";
import { StaticFonts } from "../../static/Fonts";
import { Colors } from "../../static/Colors";

export const styles = StyleSheet.create({
  component: {
    width: deviceWidth,
    height: deviceHeight / 1.05,
    flex: 1,
    backgroundColor: Colors.mainContrast,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItem: "center",
    gap: 10,
    paddingTop: 10,
  },
  titleComponent: {
    width: "100%",
    height: deviceHeight / 11,
    display: "flex",
    flexDirection: "row",
    paddingHorizontal: "4.5%",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "white",
  },
  title: {
    fontSize: deviceWidth / 20,
    fontFamily: StaticFonts[500],
    fontWeight: "bold",
  },
  iconContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
});
