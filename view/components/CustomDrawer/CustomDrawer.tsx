import React from "react";
import {
  DrawerContentComponentProps,
  DrawerContentScrollView,
  DrawerItem,
} from "@react-navigation/drawer";
import { Colors } from "../../static/Colors";
import { StaticFonts } from "../../static/Fonts";
import {
  HomeDefaultIcon,
  NotificationDefaultIcon,
  SearchIcon,
  TicketDefaultIcon,
  UserDefaultIcon,
} from "../Icons/Icons";

const CustomDrawer = ({ ...props }: DrawerContentComponentProps) => {
  return (
    <DrawerContentScrollView>
      <DrawerItem
        label="Eventos"
        onPress={() => {
          props.navigation.navigate("Home");
        }}
        labelStyle={{
          color: Colors.main,
          fontSize: 18,
          fontFamily: StaticFonts[500],
        }}
        pressColor={Colors.main}
        icon={() => <HomeDefaultIcon color={Colors.main} size={24} />}
      />
      <DrawerItem
        label="Search"
        onPress={() => props.navigation.navigate("Search")}
        labelStyle={{
          color: Colors.main,
          fontSize: 18,
          fontFamily: StaticFonts[500],
        }}
        pressColor={Colors.main}
        icon={() => <SearchIcon color={Colors.main} size={24} />}
      />
      <DrawerItem
        label="Notificações"
        onPress={() => props.navigation.navigate("Notification")}
        labelStyle={{
          color: Colors.main,
          fontSize: 18,
          fontFamily: StaticFonts[500],
        }}
        pressColor={Colors.main}
        icon={() => <NotificationDefaultIcon color={Colors.main} size={24} />}
      />
      <DrawerItem
        label="Tickets"
        onPress={() => props.navigation.navigate("Tickets")}
        labelStyle={{
          color: Colors.main,
          fontSize: 18,
          fontFamily: StaticFonts[500],
        }}
        pressColor={Colors.main}
        icon={() => <TicketDefaultIcon color={Colors.main} size={24} />}
      />
      <DrawerItem
        label="Profile"
        onPress={() => props.navigation.navigate("User")}
        labelStyle={{
          color: Colors.main,
          fontSize: 18,
          fontFamily: StaticFonts[500],
        }}
        pressColor={Colors.main}
        icon={() => <UserDefaultIcon color={Colors.main} size={24} />}
      />
    </DrawerContentScrollView>
  );
};

export default CustomDrawer;
