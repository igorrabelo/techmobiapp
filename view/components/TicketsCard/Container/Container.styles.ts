import { StyleSheet } from "react-native";
import { deviceHeight, deviceWidth } from "../../../static/Dimensions";
import { Colors } from "../../../static/Colors";

const defaultContainer = StyleSheet.create({
  default: {
    backgroundColor: "white",
    maxWidth: "90%",
    width: "100%",
    height: deviceHeight / 8,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderTopLeftRadius: deviceWidth / 60,
    borderBottomLeftRadius: deviceWidth / 60,
    paddingLeft: 10,
    elevation: 10,
  },
});

export const styles = StyleSheet.create({
  touchableOpacityContainer: {
    width: "100%",
    height: deviceHeight / 8,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  containerWithoutBorder: {
    ...defaultContainer.default,
  },
  containerWithBorder: {
    ...defaultContainer.default,
  },
  borderImageStyle: {
    height: "100%",
    width: "4%",
    backgroundColor: Colors.main,
    borderColor: Colors.main,
    // left: 5,
  },
  invisibleView: {
    width: "4%",
  },
});
