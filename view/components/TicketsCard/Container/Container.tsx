import { View, TouchableOpacity } from "react-native";
import React from "react";
import UseContainerController from "./Container.controller";
import { styles } from "./Container.styles";

type ContainerProps = {
  children: React.ReactNode;
  haveBorder?: boolean;
  onPress: () => void;
};

const Container = ({ children, haveBorder, onPress }: ContainerProps) => {
  const { States } = UseContainerController({
    haveBorder: haveBorder ?? false,
  });
  return (
    <TouchableOpacity
      onPress={onPress}
      style={styles.touchableOpacityContainer}
    >
      <View style={States.currentContainerStyle}>
        {children}
        {haveBorder ? (
          <View style={styles.borderImageStyle}></View>
        ) : (
          <View style={styles.invisibleView}></View>
        )}
      </View>
    </TouchableOpacity>
  );
};

export default Container;
