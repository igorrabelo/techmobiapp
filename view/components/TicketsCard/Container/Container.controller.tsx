import { useMemo } from "react";
import { styles } from "./Container.styles";

type UseContainerControllerProps = {
  haveBorder: boolean;
};

const UseContainerController = ({
  haveBorder,
}: UseContainerControllerProps) => {

  const renderStyleCondition = useMemo(() => {
    if (haveBorder) return styles.containerWithBorder;
    return styles.containerWithoutBorder;
  }, []);

  return {
    States: {
      currentContainerStyle: renderStyleCondition,
    },
  };
};

export default UseContainerController;
