import { EVehicleType } from "../../../../business/domain/enums/EVehicleType";

export type TVehicle = {
  type: EVehicleType;
  vehiclePlate: string;
};

export type ContentProps = {
  ticketNumber: number | string;
  ticketStatus: string;
  vehicle: TVehicle;
  createdAt: Date;
};
