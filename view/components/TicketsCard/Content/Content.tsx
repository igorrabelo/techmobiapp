import { Text, View } from "react-native";
import React from "react";
import { styles } from "./Content.styles";
import UseContentController from "./Content.controller";
import { ContentProps } from "./Types";

const Content = ({
  ticketNumber,
  ticketStatus,
  vehicle,
  createdAt,
}: ContentProps) => {
  const { Actions, Helpers } = UseContentController(vehicle);
  return (
    <View style={styles.container}>
      <View style={styles.ticketContainer}>
        <Text style={styles.ticketText}>{ticketNumber}</Text>
        <Text style={styles.timeText}>{Helpers.date.format(createdAt)}</Text>
      </View>
      <View style={styles.statusContainer}>
        <Text style={styles.statusText}>{ticketStatus}</Text>
      </View>
      <View style={styles.vehicleInformationContainer}>
        <Text style={styles.truckInformationText}>
          {Actions.onConvertVehicle()}
        </Text>
        <Text style={styles.truckInformationText}>{vehicle?.vehiclePlate}</Text>
        <Text style={styles.invertedDateText}>
          {Helpers.date.currentTime(createdAt)}
        </Text>
      </View>
    </View>
  );
};

export default Content;
