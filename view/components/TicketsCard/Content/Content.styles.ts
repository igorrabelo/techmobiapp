import { StyleSheet } from "react-native";
import { StaticFonts } from "../../../static/Fonts";
import { Colors } from "../../../static/Colors";
export const styles = StyleSheet.create({
  container: {
    width: "70%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-start",
    gap: 15,
  },
  ticketContainer: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  ticketText: {
    fontSize: 13,
    fontFamily: StaticFonts[500],
    fontWeight: "normal",
    color: "black",
  },
  timeText: {
    fontSize: 12,
    fontFamily: StaticFonts[400],
    color: Colors.info,
  },
  statusContainer: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  statusText: {
    fontSize: 13,
    fontFamily: StaticFonts[500],
  },
  vehicleInformationContainer: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  truckInformationText: {
    width: "30%",
    fontSize: 13,
    fontFamily: StaticFonts[700],
    color: "#808080A2",
  },
  invertedDateText: {
    width: "40%",
    fontSize: 12,
    fontFamily: StaticFonts[400],
    color: Colors.info,
    textAlign: "right",
  },
});
