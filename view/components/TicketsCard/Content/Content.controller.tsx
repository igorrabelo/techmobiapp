import { useCallback } from "react";
import { TVehicle } from "./Types";
import { DateFacade } from "../../../../business/service/date.facade";

const UseContentController = ({ type }: TVehicle) => {
  const dateHelper = new DateFacade();

  const convertEnumeratorVehicleIntoCorrespondingString = useCallback(() => {
    const convertedVehicles = {
      1: "Truck",
      2: "Car",
      3: "Bus",
      4: "Motorcycle",
    };
    if (convertedVehicles[type]) {
      return convertedVehicles[type];
    }
    return "";
  }, []);

  return {
    Actions: {
      onConvertVehicle: convertEnumeratorVehicleIntoCorrespondingString,
    },
    Helpers: {
      date: dateHelper,
    },
  };
};

export default UseContentController;
