import { Image, ImageSourcePropType } from "react-native";
import React from "react";
import { styles } from "./Image.styles";

type TicketImageProps = {
  source: ImageSourcePropType;
};

const TicketImage = ({ source }: TicketImageProps) => {
  return <Image style={styles.image} source={source} />;
};

export default TicketImage;
