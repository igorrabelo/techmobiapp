import { View, Text, TextInputProps } from "react-native";
import React from "react";
import { textInputStyle } from "./CustomText.styles";
import { Controller } from "react-hook-form";
import { TextInput } from "react-native-gesture-handler";

interface Props extends TextInputProps {
  control: any;
  name: string;
  errors: any;
  Icon: any;
}

const CustomText = ({ control, errors, name, Icon, ...other }: Props) => {
  return (
    <>
      <Controller
        control={control}
        render={({ field: { onChange, value } }) => (
          <View style={textInputStyle.component}>
            <View style={textInputStyle.iconContainer}>{Icon}</View>
            <TextInput
              onChangeText={onChange}
              style={textInputStyle.input}
              value={value}
              {...other}
            />
          </View>
        )}
        name={name}
      />
      {errors[name] && <Text>{errors[name].message}</Text>}
    </>
  );
};

export default CustomText;
