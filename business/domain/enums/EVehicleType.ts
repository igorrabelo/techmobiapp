export enum EVehicleType {
  Truck = 1,
  Car,
  Bus,
  Motorcycle
}
