import moment from "moment";
import { IDateFacade } from "./interfaces/date.interface";

export class DateFacade implements IDateFacade {
  private _moment = moment;

  getDate(date: Date): string {
    return this._moment(date).format("DD");
  }
  getMonth(date: Date): string {
    return this._moment(date).format("MM");
  }
  getYear(date: Date): string {
    return this._moment(date).format("YYYY");
  }
  getHours(date: Date): string {
    return this._moment(date).hour().toString();
  }
  getMinutes(date: Date): string {
    return this._moment(date).minute().toString();
  }
  getSeconds(date: Date): string {
    return this._moment(date).second().toString();
  }

  getHourToNow(previousDate: Date): string {
    const now = this._moment();
    const diff = now.diff(previousDate, "hours");
    return diff.toString();
  }
  getDaysToNow(previousDate: Date): string {
    const now = this._moment();
    const diff = now.diff(previousDate, "days");
    return diff.toString();
  }
  getMonthsToNow(previousDate: Date): string {
    const now = this._moment();
    const diff = now.diff(previousDate, "months");
    return diff.toString();
  }

  getCurrentLocation(): string {
    return this._moment().locale();
  }

  format(date: Date, location: string = this._moment().locale()): string {
    return this._moment(date).locale(location).format("L");
  }

  isValidDate(date: Date): boolean {
    return this._moment(date).isValid();
  }

  toUTC(date: Date): number {
    return this._moment.utc(date).valueOf();
  }

  differenceBetweenInMonths(olderDate: Date, newestDate: Date): string {
    const diff = this._moment(newestDate).diff(
      this._moment(olderDate),
      "months"
    );
    return diff.toString();
  }
  differenceBetweenInYears(olderDate: Date, newestDate: Date): string {
    const diff = this._moment(newestDate).diff(
      this._moment(olderDate),
      "years"
    );
    return diff.toString();
  }
  differenceBetweenInDays(olderDate: Date, newestDate: Date): string {
    const diff = this._moment(newestDate).diff(this._moment(olderDate), "days");
    return diff.toString();
  }
  currentTime(date: Date): string {
    const momentDate = this._moment(date);
    const formattedTime: string = momentDate.format("HH:mm:ss");
    return formattedTime;
  }
}
