import auth from '@react-native-firebase/auth';
import {NavigationProps} from "../../view/extensions/Navigation.type";
import { useNavigation } from "@react-navigation/native";
import { ILogOutService } from "./interfaces/logOut.interface";

export class logOutService implements ILogOutService{
    private navigation: NavigationProps = useNavigation();

    async logOut(): Promise<void> {
    await auth()
    .signOut()
    .then(() => {
        alert('Você foi deslogado');
    });
}   
}