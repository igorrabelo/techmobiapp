export interface IDateFacade {
  getDate(date: Date): string;
  getMonth(date: Date): string;
  getYear(date: Date): string;
  getHours(date: Date): string;
  getMinutes(date: Date): string;
  getSeconds(date: Date): string;
  getHourToNow(previousDate: Date): string;
  getDaysToNow(previousDate: Date): string;
  getMonthsToNow(previousDate: Date): string;
  getCurrentLocation(): string;
  format(date: Date, location: string): string;
  isValidDate(date: Date): boolean;
  toUTC(date: Date): number;
  differenceBetweenInMonths(olderDate: Date, newestDate: Date): string;
  differenceBetweenInYears(olderDate: Date, newestDate: Date): string;
  differenceBetweenInDays(olderDate: Date, newestDate: Date): string;
  currentTime(date: Date): string;
}
