export interface ILogOutService {
    logOut(): Promise<void>;
}