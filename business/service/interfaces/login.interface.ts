import { NavigationProps } from "../../../view/extensions/Navigation.type";

export interface ILoginService {
    login(email: string, password: string): Promise<void>;
}