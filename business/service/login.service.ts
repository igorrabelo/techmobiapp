import { ILoginService } from "./interfaces/login.interface";
import auth from '@react-native-firebase/auth';
import {NavigationProps} from "../../view/extensions/Navigation.type";
import { useNavigation } from "@react-navigation/native";

export class LoginService implements ILoginService{
    private navigation: NavigationProps = useNavigation();

    async login(email: string, password: string): Promise<void> {
    await auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        this.navigation.navigate('Home');
      })
      .catch(error => {
        alert('Dados Inválidos')
      });
    }
}