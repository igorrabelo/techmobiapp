import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import auth from '@react-native-firebase/auth';

import {MyButton} from '../components/MyButton';
import {styles} from './styles';
import {getLocalData, removeLocalData} from '../data/storageFileConfig';
import {
  createTableContract,
  createTableEquipment,
  insertDataIntoContract,
  insertDataIntoEquipments,
  fetchDataContracts,
  fetchDataEquipments,
  deleteDataContract,
  deleteDataEquipment,
  updateDataContract,
  updateDataEquipment,
} from '../data/sqLiteConfig';

export function HomeScreen() {
  function signOut() {
    auth().signOut();
  }

  function createTables() {
    createTableContract();
    createTableEquipment();
  }

  function insertDataInContract() {
    insertDataIntoContract('Teste SQLite');
  }

  function insertDataInEquipments() {
    insertDataIntoEquipments(123123, 1, 1);
  }

  function selectDataContract() {
    fetchDataContracts();
  }

  function selectDataEquipments() {
    fetchDataEquipments();
  }

  function deleteContract() {
    deleteDataContract(1);
  }

  function deleteEquipment() {
    deleteDataEquipment(1);
  }

  function updateContract() {
    updateDataContract('Contrato', 1);
  }

  function updateEquipment(){
    updateDataEquipment(1, 2, 0, 1);
  }

  useEffect(() => {
    // Obtendo dados do AsyncStorage
    // getLocalData('userData').then(data => {
    //   console.log('Dados obtidos do AsyncStorage:', data);
    // });

    // setTimeout(() => {
    //   removeLocalData('userData').then(() => {
    //     console.log('Dados locais removidos com sucesso');
    //   })

    //   getLocalData('userData').then((data) => {
    //     console.log('Dados obtidos do AsyncStorage:', data);
    //   });
    // }, 10000);
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>
        Essa tela só pode ser vista por usuários autenticados
      </Text>
      <MyButton onPress={signOut} title="Sair" />
      <MyButton onPress={createTables} title="Criar Tabelas" />
      <MyButton
        onPress={insertDataInContract}
        title="Inserir Dados Em Contract"
      />
      <MyButton
        onPress={insertDataInEquipments}
        title="Inserir Dados Em Equipments"
      />
      <MyButton
        onPress={selectDataContract}
        title="Selecionar Dados Contract"
      />
      <MyButton
        onPress={selectDataEquipments}
        title="Selecionar Dados Equipments"
      />
      <MyButton onPress={deleteContract} title="Deletar Um Contract" />
      <MyButton onPress={deleteEquipment} title="Deletar Um Equipment" />
      <MyButton onPress={updateContract} title="Atualizar um Contract" />
      <MyButton onPress={updateEquipment} title="Atualizar um Equipment" />
      <Text>
        <Text style={styles.coffText}>App de Testes Techmobi</Text>
      </Text>
    </View>
  );
}
