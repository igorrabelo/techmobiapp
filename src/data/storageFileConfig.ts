import AsyncStorage from '@react-native-async-storage/async-storage';

export const saveLocalData = async (key: string, data: any) => {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(data));
  } catch (error) {
    console.error('Erro salvando a data localmente!', error);
  }
};

export const getLocalData = async (key: string) => {
  try {
    const value = await AsyncStorage.getItem(key);
    return value ? JSON.parse(value) : null;
  } catch (error) {
    console.error('Erro ao buscar os dados localmente!', error);
    return null;
  }
};

export const removeLocalData = async (key: string) => {
  try {
    await AsyncStorage.removeItem(key);
  } catch (error) {
    console.error('Erro ao remover os dados localmente!', error);
  }
};
