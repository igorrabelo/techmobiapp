/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable @typescript-eslint/no-unused-vars */
import {openDatabase} from 'react-native-sqlite-storage';

// Abra o banco de dados (ou crie um novo se não existir)
const db = openDatabase({name: 'techmobi-db.db', location: 'default'});

// Função para criar uma tabela no banco de dados
export const createTableContract = () => {
  db.transaction(tx => {
    tx.executeSql(
      'CREATE TABLE IF NOT EXISTS contract (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT)',
      [],
      (tx, results) => {
        console.log('Table created successfully');
      },
      error => {
        console.error('Error creating table: ', error);
      },
    );
  });
};

export const createTableEquipment = () => {
  db.transaction(tx => {
    tx.executeSql(
      'CREATE TABLE IF NOT EXISTS equipment (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, serial_number INTEGER, active INTEGER, contract_id INTEGER)',
      [],
      (tx, results) => {
        console.log('Table created successfully');
      },
      error => {
        console.error('Error creating table: ', error);
      },
    );
  });
};

// Função para inserir dados na tabela
export const insertDataIntoContract = (name: string) => {
  db.transaction(tx => {
    tx.executeSql(
      'INSERT INTO contract (name) VALUES (?)',
      [name],
      (tx, results) => {
        console.log('Data inserted successfully');
      },
      error => {
        console.error('Error inserting data: ', error);
      },
    );
  });
};

export const insertDataIntoEquipments = (
  serial_number: int,
  active: int,
  contract_id: int,
) => {
  db.transaction(tx => {
    tx.executeSql(
      'INSERT INTO equipment (serial_number, active, contract_id) VALUES (?, ?, ?)',
      [serial_number, active, contract_id],
      (tx, results) => {
        console.log('Data inserted successfully');
      },
      error => {
        console.error('Error inserting data: ', error);
      },
    );
  });
};

// Função para recuperar dados da tabela
export const fetchDataContracts = () => {
  db.transaction(tx => {
    tx.executeSql(
      'SELECT * FROM contract',
      [],
      (tx, results) => {
        const len = results.rows.length;
        const contracts = [];
        for (let i = 0; i < len; i++) {
          const row = results.rows.item(i);
          contracts.push(row);
        }
        console.log('Contracts:', contracts);
      },
      error => {
        console.error('Error fetching data: ', error);
      },
    );
  });
};

export const fetchDataEquipments = () => {
  db.transaction(tx => {
    tx.executeSql(
      'SELECT * FROM equipment',
      [],
      (tx, results) => {
        const len = results.rows.length;
        const equipments = [];
        for (let i = 0; i < len; i++) {
          const row = results.rows.item(i);
          equipments.push(row);
        }
        console.log('Equipments:', equipments);
      },
      error => {
        console.error('Error fetching data: ', error);
      },
    );
  });
};

export const deleteDataContract = (id: number) => {
  db.transaction(tx => {
    tx.executeSql(
      'DELETE FROM contract WHERE id = ?',
      [id],
      (tx, results) => {
        console.log('Registro Deletado!');
      },
      error => {
        console.error('Error deleting data: ', error);
      },
    );
  });
};

export const deleteDataEquipment = (id: number) => {
  db.transaction(tx => {
    tx.executeSql(
      'DELETE FROM equipment WHERE id = ?',
      [id],
      (tx, results) => {
        console.log('Registro Deletado!');
      },
      error => {
        console.error('Error deleting data: ', error);
      },
    );
  });
};

export const updateDataContract = (name: string, id: int) => {
  db.transaction(tx => {
    tx.executeSql(
      'UPDATE contract SET name = ? WHERE id = ?',
      [name, id],
      (tx, results) => {
        console.log('Registro Atualizado');
      },
      error => {
        console.error('Error updating data: ', error);
      },
    );
  });
};

export const updateDataEquipment = (serial_number: int, active: int, contract_id: int, id: int) => {
    db.transaction(tx => {
      tx.executeSql(
        'UPDATE equipment SET serial_number = ?, active = ?, contract_id = ? WHERE id = ?',
        [serial_number, active, contract_id, id],
        (tx, results) => {
          console.log('Registro Atualizado');
        },
        error => {
          console.error('Error updating data: ', error);
        },
      );
    });
  };