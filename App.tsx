import "react-native-gesture-handler";
import { Provider } from "react-redux";
import { NavigationContainer } from "@react-navigation/native";
import { GestureHandlerRootView } from "react-native-gesture-handler";
import { QueryClientProvider, QueryClient } from "react-query";
import store from "./view/store/store";
import React, {useEffect, useState} from 'react';
import DrawerRouter from "./view/router/Drawer/Drawer.router";
import auth, {FirebaseAuthTypes} from '@react-native-firebase/auth';

const queryClient = new QueryClient();

export default function App() {
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState<FirebaseAuthTypes.User | null>(null);

  useEffect(() => {
    const unsubscribe = auth().onAuthStateChanged(_user => {
      if (initializing) {
        setInitializing(false);
      }
      setUser(_user);
    });

    return unsubscribe;
  }, [initializing]);
  return (
    <QueryClientProvider client={queryClient}>
      <Provider store={store}>
        <GestureHandlerRootView
          style={{
            flex: 1,
          }}
        >
          <NavigationContainer>
            <DrawerRouter />
          </NavigationContainer>
        </GestureHandlerRootView>
      </Provider>
    </QueryClientProvider>
  );
}
